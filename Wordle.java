import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Wordle {
    final static String COLOR_WHITE = "\u001B[0m";
    final static String COLOR_GREEN = "\u001B[32m";
    final static String COLOR_YELLOW = "\u001B[33m";

    final static String WHITE = "white";
    final static String GREEN = "green";
    final static String YELLOW = "yellow";

    static Scanner input = new Scanner(System.in);

  /*  public static void main(String[] args) {
        // System.out.println(generateWord());
        //String[] testColours = new String[]{"green", "white", "yellow", "white", "yellow"};
        //presentResults("PRINT", testColours);
        String generatedWord = generateWord();
        runGames(generatedWord);
    } */

    public static String generateWord() {
        String[] words = new String[]{"JUICE", "FLASK", "ZEBRA", "GLINT", "REALM",
                "BUCKY", "COBRA", "MANGO", "TRAIN", "DRAFT", "PRISM", "SWINE", "STACK", "BUDGE",
                "GRIME", "GLIDE", "CHIRP", "BLUSH", "PLUMB", "SWANK", "BRAWN", "QUIRK", "LAXER",
                "FROST", "CLASP", "BRINY", "PETAL", "CRUMB", "SYRUP", "SKULK", "GROVE", "FROWN",
                "CRUST", "BERYL", "JOUST", "NYMPH", "PLUCK", "VIXEN", "HEFTY", "CHIVE", "PRISM",
                "POLAR", "CLEFT", "LYRIC", "FLUKE", "QUASH", "ADOBE", "LIVEN", "SKIFF", "PLAID",
                "RADIX", "BONGO", "JOUST", "NEXUS", "SAVOR", "GRIST", "SWISH", "PLUMB", "FLINT",
                "DRUID", "CAMEL", "WALTZ", "JOKER", "FORGE", "PLUME", "SQUID", "HOUND", "SMIRK",
                "PULSE", "FAUNA", "SLANT", "CHARM", "OVERT", "FLUSH", "GUSTO", "SMITE", "BLEEP",
                "PRISM", "WINCE", "BRAVO", "BULGE", "OPTIC", "GLIDE", "PLUCK", "POLAR", "EXALT",
                "SWINE", "NYMPH", "FROWN", "WALTZ", "BLINK", "SMIRK", "OVERT", "VINYL", "CHARM"};
        Random random = new Random();
        int index = random.nextInt(words.length);
        return words[index];
    }

    public static boolean letterInWord(String word, char letter) {
        for (int i = 0; i < word.length(); i++) {
            if (letterInSlot(word, letter, i)) {
                return true;
            }
        }

        return false;
        //return word.indexOf(letter) != -1;
    }

    public static boolean letterInSlot(String word, char letter, int position) {
        return word.charAt(position) == letter;
    }

    public static String[] guessWord(String answer, String guess) {
        if (answer.equals(guess)) {
            return new String[]{GREEN, GREEN, GREEN, GREEN, GREEN};
        }

        String[] colorWords = new String[5];

        for (int i = 0; i < guess.length(); i++) {
            char letterInGuess = guess.charAt(i);

            if (letterInSlot(answer, letterInGuess, i)) {
                colorWords[i] = GREEN;
            } else if (letterInWord(answer, letterInGuess)) {
                colorWords[i] = YELLOW;
            } else {
                colorWords[i] = WHITE;
            }
        }

        return colorWords;
    }

    public static void presentResults(String word, String[] colours) {

        for (int i = 0; i < word.length(); i++) {
            System.out.print(convertColor(colours[i]) + word.charAt(i));
            if (word.length() - 1 != i) {
                System.out.print(" ");
            }
        }
        System.out.println(COLOR_WHITE);
    }

    public static String readGuess() {
        String guess = "";

        while (guess.length() != 5 || !areAllLetters(guess)) {
            System.out.print("Enter a five letter word: ");
            guess = input.nextLine();
            if (guess.length() == 5 && areAllLetters(guess)) {
                //return guess.toUpperCase();
                guess = toUpperCaseTheLongWay(guess);
            } else {
                System.out.println("Invalid! ");
            }
        }
        return guess;
    }

    public static void runGames(String word) {
        boolean win = false;
        for (int i = 0; i < 6; i++) {
            String guess = readGuess();
            String[] colors = guessWord(word, guess);
            presentResults(guess, colors);
            if (colors[0].equals(GREEN) && colors[1].equals(GREEN) && colors[2].equals(GREEN) && colors[3].equals(GREEN) && colors[4].equals(GREEN)) {
                win = true;
                break;
            }
        }
        if (win) {
            System.out.println("You win!");
        } else {
            System.out.println("Try again!");
        }

    }

    public static String convertColor(String color) {
// switch is for cases with one comparison with == for an if statement
        switch (color) {
            case GREEN:
                return COLOR_GREEN;
            case WHITE:
                return COLOR_WHITE;
            case YELLOW:
                return COLOR_YELLOW;

        }
        return null;
    }

    public static boolean areAllLetters(String text) {
        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            if (!isLetter(letter)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isLetter(char letter) {
        return isLowerCaseLetter(letter) || (letter >= 'A' && letter <= 'Z');
    }

    public static String toUpperCaseTheLongWay(String text) {
        String upperCaseWord = "";

        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            if (isLowerCaseLetter(letter)) {
                upperCaseWord += (char) (letter - 32);
            } else {
                upperCaseWord += letter;
            }

        }

        return upperCaseWord;
    }

    public static boolean isLowerCaseLetter(char letter) {
        return letter >= 'a' && letter <= 'z';
    }
}



